package main

import (
	"fmt"
	"time"
)

func main() {
	server := Server{}
	server.Status = make(chan string)

	go func() {
		for {
			newStatus := <-server.Status
			fmt.Println("New Status", newStatus)
		}
	}()

	server.TurnOn()
	server.Restart()
	server.Shutdown()

	fmt.Scanln()
}

// Server contains event listeners
type Server struct {
	Status chan string
}

// TurnOn changes the server instance's status
// after 2 sec to "turn on"
func (s *Server) TurnOn() {
	time.Sleep(time.Second * 2)
	s.Status <- "turned on"
}

// Restart changes the server instance's status
// after 1 sec to 'rebooting'
// and then after 2 sec to 'turned on'
func (s *Server) Restart() {
	time.Sleep(time.Second * 1)
	s.Status <- "rebooting"

	time.Sleep(time.Second * 2)
	s.Status <- "turned on"
}

// Shutdown changes the server instance's status
// after 2 sec to 'turned off'
func (s *Server) Shutdown() {
	time.Sleep(time.Second * 2)
	s.Status <- "turned off"
}
